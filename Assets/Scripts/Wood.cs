﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Wood : MonoBehaviour
{
    private void Awake()
    {
        try
        {
            GetComponentInChildren<SpriteRenderer>().sprite = ((Resources.LoadAll<Sprite>("Sprites/Tiles/Decoration 1")).Single(s => s.name == ("Decoration 1_" + Random.Range(2, 4))));
        }
        catch { };
    }
}
