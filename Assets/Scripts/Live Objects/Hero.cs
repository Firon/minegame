﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hero : Creatures
{

    new private HeroCamera camera;
    private Rigidbody2D bodyPhysic;
    new private EdgeCollider2D collider;
    private SpriteRenderer spriteRenderer;

    private float shiftSpeed = 0;

    private float speedResult
    {
        get { return movementSpeed + shiftSpeed + modifiedSpeed; }
    }


    private float racing;
    public float Racing
    {
        get { return racing; }
    }

    private Text text;

    private float jumpForce = 7.5F;

    protected void Awake()
    {
        bodyPhysic = transform.GetComponent<Rigidbody2D>();
        collider = transform.GetComponent<EdgeCollider2D>();
        spriteRenderer = transform.GetComponentInChildren<SpriteRenderer>();

        // Задаем базовые характеристики персонажа.

        movementSpeed = 3;
        text = transform.Find("/WorldSpace/txt/Text").GetComponent<Text>();

        camera = transform.Find("/WorldSpace/HeroCamera").GetComponent<HeroCamera>();

    }

    protected override void Update()
    {
        base.Update();
        if (Input.GetButton("Horizontal")) Move();


        int num = 0;
        Collider2D[] collision = Physics2D.OverlapCircleAll(new Vector3(transform.position.x, transform.position.y - 0.5F), 0.05F);

        foreach (Collider2D colliderElement in collision) if (colliderElement.GetComponent<Ground>() != null) num++;

        onTheGround = num >= 1;

        if (onTheGround) bodyPhysic.velocity = new Vector2(0, 0);

        text.text = "speed " + racing + "\nMax Speed " + movementSpeed;
    }

    protected void FixedUpdate()
    {
        if (Input.GetButtonUp("Horizontal")) racing = 0;
        if (Input.GetKey(KeyCode.Space) && onTheGround) Jump();
    }

    private void Move()
    {
        Vector3 direction = new Vector3();

        //if (direction.x != 0 && direction.x != Input.GetAxis("Horizontal"))
        //{
        //    Vector2[] newPoints = collider.points;
        //    for (int i = 0; i < newPoints.Length; i++) newPoints[i].Set(collider.points[i].x * direction.x, collider.points[i].y);
        //    collider.points = newPoints;
        //}

        direction.x = Input.GetAxis("Horizontal");

        //Debug.Log(collider.points[1].x + " <- это говно не меняется " + collider.points[1].x * direction.x);

        // Поворачиваем персонажа, в сторону движения.
        if (direction.x < 0) spriteRenderer.flipX = true;
        else spriteRenderer.flipX = false;

        // Нагружаем макс. скорость модификаторами.
        if (Input.GetKey(KeyCode.LeftShift)) shiftSpeed = movementSpeed * 0.5F;
        else shiftSpeed = 0;

        if (racing <= speedResult) racing += 0.15F;
        if (!Input.GetKey(KeyCode.LeftShift) && racing > speedResult) racing = speedResult;

        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, racing * Time.deltaTime);

        BackgroundMove.SetDirection(direction);


    }

    private void Jump()
    {
        onTheGround = false;
        bodyPhysic.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }

    //// Первая фун.
    //private int[] CreateArray(int range)
    //{
    //    int[] arr = new int[range];
    //    return arr; // Возвращаем массив.
    //}

    //// Вторая фун.
    //private void ListArray(int[] inpArray) // Массив в качестве аргумента.
    //{
    //    for (int i = 0; i < inpArray.Length; i++) // Выводим его.
    //    {
    //        Debug.Log(inpArray[i]);
    //    }
    //}

    //private void Main()
    //{
    //    // Вызываем вторую функцию и передаем в неё первую функцию.
    //    ListArray(CreateArray(5));
    //}

}
