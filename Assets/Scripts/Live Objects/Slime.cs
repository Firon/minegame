﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Monsters
{
    private Rigidbody2D body;
    new private EdgeCollider2D collider;
    private SpriteRenderer sprite;

    private Animator animator;

    private bool isNotNewDirect = true;
    private bool isNotNewState = true;

    private Vector3 directionMove;

    private StateMonster stateMonster = StateMonster.idly;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        collider = GetComponent<EdgeCollider2D>();
        movementSpeed = 1.5F;

        directionMove = new Vector3(1, 0, 0);

        sprite = GetComponentInChildren<SpriteRenderer>();

        animator = GetComponent<Animator>();

        //sprite.color = new Color(Random.Range(0, 0.99F), 1, Random.Range(0, 0.99F)); 
        float size;
        transform.localScale.Set(size = Random.Range(0.8F, 1.3F), size, 1);
    }

    protected override void Update()
    {
        base.Update();

        if (stateMonster != StateMonster.Attak && isNotNewState) StartCoroutine(NewState());

        if(stateMonster == StateMonster.Move) Move();

        int num = 0;
        Collider2D[] collision = Physics2D.OverlapCircleAll(new Vector3(transform.position.x, transform.position.y - 0.5F), 0.05F);
        foreach (Collider2D colliderElement in collision) if (colliderElement.GetComponent<Ground>() != null) num++;
        onTheGround = num >= 1;
        if (onTheGround && stateMonster != StateMonster.idly) Jump();


        sprite.flipX = directionMove.x == -1;

        if (transform.position.y < -50) Destroy(this.gameObject);
    }

    protected void Move()
    {
        if (isNotNewDirect) StartCoroutine(NewDirectionMove());
        transform.position = Vector3.MoveTowards(transform.position, transform.position + directionMove, movementSpeed * Time.deltaTime);
    }

    private void Jump()
    {
        int num = 0;
        Collider2D[] collision = Physics2D.OverlapCircleAll(new Vector3(transform.position.x + directionMove.x, transform.position.y), 0.05F);
        foreach (Collider2D colliderElement in collision) if (colliderElement.GetComponent<Ground>() != null) num++;
        if (num >= 1) body.AddForce(transform.up * 1.5F, ForceMode2D.Impulse);
    }

    private IEnumerator NewDirectionMove()
    {
        isNotNewDirect = false;
        yield return new WaitForSeconds(3.5F);
        directionMove = new Vector3(((Random.Range(0, 2) == 0) ? -1 : 1), directionMove.y, directionMove.z);
        isNotNewDirect = true;
    }

    private IEnumerator NewState()
    {
        animator.speed = Random.Range(0.5F, 1F);
        isNotNewState = false;
        yield return new WaitForSeconds(Random.Range(2, 5));
        stateMonster = (StateMonster)Random.Range(0, 2);
        isNotNewState = true;
    }

    private enum StateMonster
    {
        idly,
        Move,
        Attak
    }
}
