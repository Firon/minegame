﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Creatures : MonoBehaviour
{
    // Уровень.
    protected int level;
    public int Level
    {
        get { return level; }
        set { level = value; }
    }

    protected int exp;
    public int Exp
    {
        get { return exp; }
        set { exp = value; }
    }

    // Максимальное здоровье.
    protected int maxHelth;
    public int MaxHelth
    {
        get { return maxHelth; }
        set { maxHelth = value; }
    }

    // Показатель здоровья на данный момент.
    protected float helth;
    public float Helth
    {
        get { return helth; }
        set { helth = value; }
    }

    // Максимальное кол-во выносливости.
    protected int maxStamina;
    public int MaxStamina
    {
        get { return maxStamina; }
        set { maxStamina = value; }
    }

    // Показатель выносливости на данный момент. 
    protected int stamina;
    public int Stamina
    {
        get { return stamina; }
        set { stamina = value; }
    }

    // Максимальное кол-во маны.
    protected int maxMana;
    public int MaxMana
    {
        get { return maxMana; }
        set { maxMana = value; }
    }

    // Флаг, существо на земле.
    protected bool onTheGround = false;

    // Показатель маны.
    protected int mana;
    public int Mana
    {
        get { return mana; }
        set { mana = value; }
    }

    // Скорость передвижения.
    protected float movementSpeed;
    public float MovementSpeed
    {
        get { return movementSpeed; }
        set { movementSpeed = value; }
    }

    protected float modifiedSpeed;
    public float ModifiedSpeed
    {
        get { return modifiedSpeed; }
        set { modifiedSpeed = value; }
    }

    // Показател брони.
    protected float armor;
    public float Armor
    {
        get { return armor; }
        set { armor = value; }
    }

    // Интервальный урон.
    //Мин. урон.
    protected int minDamage;

    // Макс. урон.
    protected int maxDamage;

    protected int midDamage;

    // Еденица поглощения (на 1 еденицу армора).
    protected const float unitAbsorption = 0.02F;

    public int absorption
    {
        // В процентах.
        get { return (int)(armor * unitAbsorption); }
    }

    // Метод получения урона.
    protected virtual void ReceivedDamage(int inpDamage)
    {
        // здоровье
        helth -= inpDamage * ((100 -  absorption) / 100);
    }

    // Метод смерти.
    protected virtual void Die()
    {
        Destroy(this.gameObject);
    }

    protected virtual void Update()
    {
        // Высчитываем средний урон.
        midDamage = maxDamage = minDamage;

    }
}
