﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Ground : MonoBehaviour
{
    private GameObject decorationObject;

    private bool isGress;
    public bool IsGress
    {
        get { return IsGress; }
        set { isGress = value; }
    }

    private void Update()
    {
        if (isGress)
        {
            // Проверка на сущ. блока слева.
            Collider2D[] colliderLeft = Physics2D.OverlapCircleAll(new Vector2(transform.position.x - transform.lossyScale.x, transform.position.y), 0.1F);
            if(colliderLeft.Length == 0)
            {
                this.transform.GetComponentInChildren<SpriteRenderer>().sprite = (Resources.LoadAll<Sprite>("Sprites/Tiles/GreenLand")).Single(s => s.name == ("GreenLand_0"));
            }

            // Проверка на сущ. блока справа.
            Collider2D[] colliderRight = Physics2D.OverlapCircleAll(new Vector2(transform.position.x + transform.lossyScale.x, transform.position.y), 0.1F);
            if(colliderRight.Length == 0)
            {
                this.transform.GetComponentInChildren<SpriteRenderer>().sprite = (Resources.LoadAll<Sprite>("Sprites/Tiles/GreenLand")).Single(s => s.name == ("GreenLand_4"));
            }
            isGress = false;
            int chanse = Random.Range(0, 30);

            if(chanse == 15)
            {
                GameObject wood = Resources.Load("Prefabs/GreenLand/Wood") as GameObject;
                decorationObject = Instantiate(wood, new Vector3(transform.position.x, transform.position.y + (transform.lossyScale.y / 2) - 0.1F, 2), Quaternion.identity);
            }
            else if(chanse > 5 && chanse < 20)
            {
                GameObject wood = Resources.Load("Prefabs/GreenLand/Grass") as GameObject;
                decorationObject = Instantiate(wood, new Vector3(transform.position.x, transform.position.y + (transform.lossyScale.y / 2) - 0.1F, 2), Quaternion.identity);
            }

            if(Random.Range(0, 50) == 25)
            {
                GameObject slime = Resources.Load("Prefabs/GreenLand/Monsters/Slime") as GameObject;
                Instantiate(slime, new Vector3(transform.position.x, transform.position.y + (transform.lossyScale.y / 2) - 0.1F, 2), Quaternion.identity);
            }
        }
    }
}