﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMove : MonoBehaviour
{
    // Задний фон.
    // Первый слой, 1 кадр.
    private GameObject BG1_Frame1;
    // Первый слой, 2 кадр.
    private GameObject BG1_Frame2;

    // Второй слой, 1 кадр.
    private GameObject BG2_Frame1;
    // Второй слой, 2 кадр.
    private GameObject BG2_Frame2;

    // Третий слой, дальний фон.
    // Он статичен, не имеет движения.
    private GameObject BG3;

    // Направление движения персонажа.
    static private Vector3 heroDirection;

    // Сам персонаж.
    private Hero hero;

    // Здесь хранится расположение персонажа.
    private Vector3 bufferPosition;

    private void Awake()
    {
        // Находим слои для заднего фона.
        // Первый слой, ближний.
        BG1_Frame1 = transform.Find("/WorldSpace/HeroCamera/BG/Level1_1").gameObject;
        BG1_Frame2 = transform.Find("/WorldSpace/HeroCamera/BG/Level1_2").gameObject;

        // Второй слой, центральный.
        BG2_Frame1 = transform.Find("/WorldSpace/HeroCamera/BG/Level2_1").gameObject;
        BG2_Frame2 = transform.Find("/WorldSpace/HeroCamera/BG/Level2_2").gameObject;

        // Третий слой, дальний.
        BG3 = transform.Find("/WorldSpace/HeroCamera/BG/Level3").gameObject;

        // Находим персонажа.
        hero = transform.Find("/WorldSpace/Character").GetComponent<Hero>();
    }

    private void Update()
    {
        MoveBackGround(heroDirection);
    }

    static public void SetDirection(Vector3 direction) { heroDirection = direction; }

    private void MoveBackGround(Vector3 direction)
    {
        // Проверяем на то, что пепрсонаж сдвинулся с места.
        if(Mathf.Abs(hero.transform.position.x - bufferPosition.x) > (0.1F * 0.1F))
        {
            // Двигаем первый слой.
            BG1_Frame1.transform.localPosition = Vector3.MoveTowards(BG1_Frame1.transform.localPosition, BG1_Frame1.transform.localPosition + new Vector3(-direction.x, 0, 0), (hero.Racing / 2) * Time.deltaTime);
            BG1_Frame2.transform.localPosition = Vector3.MoveTowards(BG1_Frame2.transform.localPosition, BG1_Frame2.transform.localPosition + new Vector3(-direction.x, 0, 0), (hero.Racing / 2) * Time.deltaTime);

            // Проверяем на то, что кадр дошел до конца и передвинаем его в самое начало.
            if (BG1_Frame2.transform.localPosition.x > 14) BG1_Frame2.transform.localPosition = new Vector3(-13.8F, BG1_Frame2.transform.localPosition.y, BG1_Frame2.transform.localPosition.z);
            if (BG1_Frame2.transform.localPosition.x < -14) BG1_Frame2.transform.localPosition = new Vector3(13.8F, BG1_Frame2.transform.localPosition.y, BG1_Frame2.transform.localPosition.z);
            // Проверяем на то, что кадр дошел до конца и передвинаем его в самое начало.
            if (BG1_Frame1.transform.localPosition.x > 14) BG1_Frame1.transform.localPosition = new Vector3(-13.8F, BG1_Frame1.transform.localPosition.y, BG1_Frame1.transform.localPosition.z);
            if (BG1_Frame1.transform.localPosition.x < -14) BG1_Frame1.transform.localPosition = new Vector3(13.8F, BG1_Frame1.transform.localPosition.y, BG1_Frame1.transform.localPosition.z);


            // Двигаем второй слой.
            BG2_Frame1.transform.localPosition = Vector3.MoveTowards(BG2_Frame1.transform.localPosition, BG2_Frame1.transform.localPosition + new Vector3(-direction.x, 0, 0), (hero.Racing / 3) * Time.deltaTime);
            BG2_Frame2.transform.localPosition = Vector3.MoveTowards(BG2_Frame2.transform.localPosition, BG2_Frame2.transform.localPosition + new Vector3(-direction.x, 0, 0), (hero.Racing / 3) * Time.deltaTime);
            // Проверяем на то, что кадр дошел до конца и передвинаем его в самое начало.
            if (BG2_Frame2.transform.localPosition.x > 14) BG2_Frame2.transform.localPosition = new Vector3(-13.8F, BG2_Frame2.transform.localPosition.y, BG2_Frame2.transform.localPosition.z);
            if (BG2_Frame2.transform.localPosition.x < -14) BG2_Frame2.transform.localPosition = new Vector3(13.8F, BG2_Frame2.transform.localPosition.y, BG2_Frame2.transform.localPosition.z);
            // Проверяем на то, что кадр дошел до конца и передвинаем его в самое начало.
            if (BG2_Frame1.transform.localPosition.x > 14) BG2_Frame1.transform.localPosition = new Vector3(-13.8F, BG2_Frame1.transform.localPosition.y, BG2_Frame1.transform.localPosition.z);
            if (BG2_Frame1.transform.localPosition.x < -14) BG2_Frame1.transform.localPosition = new Vector3(13.8F, BG2_Frame1.transform.localPosition.y, BG2_Frame1.transform.localPosition.z);
            
        }
        // Запоминаем расположение персонажа на прошлом кадре.
        bufferPosition = hero.transform.position;
    }

    private List<int[]> listArr = new List<int[]>();

}
