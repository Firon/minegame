﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroCamera : MonoBehaviour
{
    private Hero hero;

    private float speed;

    private float racing;

    private void Awake()
    {
        hero = transform.Find("/WorldSpace/Character").GetComponent<Hero>();
    }

    private void Update()
    {
        Vector3 vLen = hero.transform.position - transform.position;
        float fLen = vLen.sqrMagnitude;

        if (speed != hero.MovementSpeed) speed = hero.MovementSpeed;
        racing = fLen / 1.5F;
        transform.position = Vector2.Lerp(transform.position, new Vector3(hero.transform.position.x, hero.transform.position.y + 0.7F, hero.transform.position.z), racing * Time.deltaTime);
    }
}