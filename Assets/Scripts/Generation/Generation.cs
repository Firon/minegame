﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Generation : MonoBehaviour
{
    public Ground dirtPrefab;
    public Ground grassPrefab;

    private Sprite[] sprites;

    int minX = -100;
    int maxX = 100;
    int minY = -10;
    int maxY = 30;

    PerlinNoise noise;

    void Start()
    {
        sprites = Resources.LoadAll<Sprite>("Sprites/Tiles/GreenLand");
        noise = new PerlinNoise(Random.Range(1000000, 10000000));
        Regenerate();
    }

    private void Regenerate()
    {

        float width = dirtPrefab.transform.lossyScale.x;
        float height = dirtPrefab.transform.lossyScale.y;

        

        for (int i = minX; i < maxX; i++)
        {//columns (x values
            int columnHeight = 2 + noise.getNoise(i - minX, maxY - minY - 2);
            for (int j = minY; j < minY + columnHeight; j++)
            {//rows (y values)
                Ground block;
                bool isGress = (j == minY + columnHeight - 1) ? true : false;

                // Поверхность земли.
                if (isGress)
                {
                    block = grassPrefab;
                    int index = Random.Range(1, 4);
                    
                    block.transform.GetComponentInChildren<SpriteRenderer>().sprite = sprites.Single(s => s.name == ("GreenLand_" + index));

                }
                // Под землей.
                else
                {
                    block = dirtPrefab;
                    if (Random.Range(0, 10) >= 5)
                    { int index = Random.Range(5, 11);
                        if(index != 6 && index != 8)block.transform.GetComponentInChildren<SpriteRenderer>().sprite = sprites.Single(s => s.name == ("GreenLand_" + index));
                    }
                    else block.transform.GetComponentInChildren<SpriteRenderer>().sprite = sprites.Single(s => s.name == ("GreenLand_7"));
                }

                Ground clone = Instantiate(block, new Vector3(i * width, j * height, 2), Quaternion.identity) as Ground;
                clone.IsGress = isGress;
                clone.transform.parent = transform;
            }
        }
    }
}